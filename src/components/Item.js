/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Button, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import image from '../images/vino.jpg';


const Item = ({producto}) => {
  return (
    <>
      <Card border="secondary" style={{ width: "18rem" }} >
      <Card.Header >{producto.title}</Card.Header>
      <Card.Img variant="top" src={image} />
        <Card.Body >
          <Card.Text>
            <h5><b>Descripcion:</b></h5>
            <h6>{producto.description}</h6>
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Button variant="outline-secondary" name="reset" style={{ width: "16rem" }}  href={`/articulo/${producto.id}`}>ver detalle</Button>
        </Card.Footer>
      </Card>
    </>
  );
};

export default Item;
