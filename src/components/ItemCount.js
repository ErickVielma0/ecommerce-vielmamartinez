import { useState } from "react";
import { IoAddOutline } from "react-icons/io5";
import { MdRestartAlt } from "react-icons/md";
import { GrSubtract } from "react-icons/gr";
import { Button, ButtonGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { toast } from "react-toastify";

const ItemCount = ({stock,initial,onAdd}) => {
  const [contador, setContador] = useState(initial);
  const [itemStock, setItemStock] = useState(stock);
  const [disableAdd, setDisableAdd] = useState(false);
  const [disableSub, setDisableSub] = useState(true);
  const [disableRes, setDisableRes] = useState(true);
  const [disableCon, setDisableCon] = useState(true);
  const [disableAddItems, setDisableAddItems] = useState(true);

  const add = () => {
    setContador(contador + 1);
    if (contador === itemStock - 1) {
      setDisableAdd(true);
    }
    if (contador >= 0) {
      setDisableRes(false);
      setDisableSub(false);
      setDisableAddItems(false);
      setDisableCon(false);
    }
  };

  const substract = () => {
    setContador(contador - 1);
    if (contador <= itemStock) {
      setDisableAdd(false);
      setDisableRes(true);
      setDisableAddItems(true);
      setDisableCon(true);
    }
    if (contador === 0) {
      setDisableRes(true);
      setDisableSub(true);
      setDisableAddItems(true);
      setDisableCon(true);
    }
    if (contador <= 0) {
      setContador(0);
      setDisableRes(true);
      setDisableAddItems(true);
      setDisableSub(true);
      setDisableCon(true);
    }
  };

  const reset = () => {
    setContador(0);
    if (contador <= itemStock) {
      setDisableAdd(false);
      setDisableAddItems(true);
      setDisableSub(true);
      setDisableRes(true);
      setDisableCon(true);
    }
    if (contador === 0) {
      setDisableRes(true);
      setDisableSub(true);
      setDisableAddItems(true);
      setDisableCon(true);
    }
    if (contador <= 0) {
      setDisableRes(true);
      setDisableAddItems(true);
      setDisableSub(true);
      setDisableCon(true);
    }
  };

  const confirm = () => {
    toast(`Se han agregado `+ contador + ` articulo(s) al carrito`, {hideProgressBar:true, closeOnClick: true, autoClose: 1000});
    setContador(0);
    setDisableRes(true);
    setDisableAddItems(true);
    setDisableSub(true);
    setDisableCon(true);
    setItemStock(itemStock-contador);
    onAdd(contador)
  };

  return (
    <>
      <p>
        Cantidad: {contador} unidad(es) {itemStock} Disponibles
      </p>
      <br />
      <ButtonGroup aria-label="Basic example" size="sm" style={{ width: "16rem", margin:"auto" }}>
        <Button
          variant="outline-danger"
          name="substract"
          onClick={substract}
          disabled={disableSub}
          style={{ width: "3rem" }}
        >
          <GrSubtract />
        </Button>
        <Button
          variant="outline-secondary"
          name="reset"
          onClick={reset}
          disabled={disableRes}
          style={{ width: "3rem", margin:"auto" }}
        >
          <MdRestartAlt />
        </Button>
        <Button
          variant="outline-success"
          name="add"
          onClick={add}
          disabled={disableAdd}
          style={{ width: "3rem" }}
        >
          <IoAddOutline />
        </Button>
      </ButtonGroup>

      <br />
      <br />
      <Button
        variant="outline-secondary"
        name="onAdd"
        onClick={confirm}
        style={{ width: "16rem" }}
        disabled={disableAddItems}
      >
        Agregar al Carrito
      </Button>
      <br />
      <br />
      <Button
        variant="outline-secondary"
        style={{ width: "16rem" }}
        href={`/Sale`}
        disabled={disableCon}
      >
        Confirmar compra
      </Button>
    </>
  );
};

export default ItemCount;
