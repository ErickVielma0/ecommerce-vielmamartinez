import Footer from "./components/Footer";
import Header from "./components/Header";
import Main from "./components/Main";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import MiProvider from "./components/CartContext"

function App() {
  return (
    <MiProvider>
      <BrowserRouter>
        <Header />
        <Main nombre="Erick Eduardo" apellido="Vielma Martínez"></Main>
        <Footer />
        <ToastContainer/>
      </BrowserRouter>
      </MiProvider>
  );
}

export default App;
