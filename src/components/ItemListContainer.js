import ItemList from "./ItemList";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

let productosIniciales = [
  {
    id: 1,
    title: "Articulo 1",
    description: "Articulo 1 Desc",
    price: 100,
    pictureUrl: "./images/articulo1",
    stock: Math.floor(Math.random() * 25),
    categoria: 1
  },
  {
    id: 2,
    title: "Articulo 2",
    description: "Articulo 2 Desc",
    price: 100,
    pictureUrl: "./images/articulo2",
    stock: Math.floor(Math.random() * 25),
    categoria: 2
  },
  {
    id: 3,
    title: "Articulo 3",
    description: "Articulo 3 Desc",
    price: 100,
    pictureUrl: "./images/articulo3",
    stock: Math.floor(Math.random() * 25),
    categoria: 2
  }
];

const ItemListContainer = (props) => {
  
  const [productos, setProductos] = useState([]);
  const {id} = useParams()

  useEffect(() => {
    const getItem = new Promise((res) => {
      setTimeout(() => {
        toast("Cargando datos", {hideProgressBar:true, closeOnClick: true, autoClose: 1000});
        res(productosIniciales);
      }, 2000);
    });
    getItem
      .then((respuesta) => {
        setProductos(id?productosIniciales.filter(producto=>producto.categoria === +id) : respuesta);
      })
      .catch((e) => {
        console.log(e);
        toast.error("Error al cargar los productos");
      })
      .finally(() => {
        toast("Datos cargados", {hideProgressBar:true, closeOnClick: true, autoClose: 3000});
      });
  },[id]);

  return (
    <>
      <ItemList productos={productos} />
    </>
  );
};

export default ItemListContainer;
