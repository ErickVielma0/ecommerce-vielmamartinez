/* eslint-disable jsx-a11y/anchor-is-valid */
import { MdShoppingCart } from "react-icons/md";

const Navbar = () => {
  return (
    <nav>
      <a href="/carrito"><MdShoppingCart /></a>
      <a href="/categoria/2">Clásicos</a>
      <a href="/categoria/1">Ofertas</a>
      <a href="/">Inicio</a>
    </nav>
  )
}

export default Navbar