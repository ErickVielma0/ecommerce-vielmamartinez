import {React, useContext} from 'react'
import { context } from "./CartContext"

const Carrito = () => {
  const resultado = useContext(context);
  const carrito = resultado.carrito;
  const removeItem = resultado.borrarDelCarrito;

  console.log(resultado)

  const handleBorrar = () => {
      console.log("borrar")
      removeItem();
  }

  return (
      <>
          <h2>Carrito</h2>
          {carrito.map(item => (
              <div key={item.id}>
                  <h3>{item.titulo}</h3>
                  <p>{item.descripcion}</p>
                  <p>{item.precio} x {item.cantidad}</p>
                  <p>Total : ${item.precio * item.cantidad}</p>
                  <button onClick={handleBorrar}>borrar</button>
              </div>
          ))}
          {/* <Link>checkout</Link> */}
      </>
  )
}

export default Carrito