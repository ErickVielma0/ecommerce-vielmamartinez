import { createContext, useState } from "react";

export const context = createContext();
const { Provider } = context;

const MiProvider = ({ children }) => {
  const [carrito, setCarrito] = useState([]);
  const [total, setTotal] = useState(0);
  const [quantity, setQuantity] = useState(0);

  const removeItem = (id) => {};

  const addItem = (item, quantity) => {
    if (isInCart()) {
    } else {
      const copia = carrito.slice(0);
    }
  };

  const clear = () => {
    setCarrito([]);
  };

  const isInCart = (id) => {
    //recorre todo el carrito en busca del producto con id "id"
    //devuelve true si el producto existe
    //devuelve false si el producto no existe
  };

  const contextValue = {
    total: total,
    carrito: carrito,
    clear: clear,
    addItem: addItem,
  };

  return <Provider value={contextValue}>{children}</Provider>;
};

export default MiProvider;
