import Item from "./Item";
import { Form, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const ItemList = ({ productos }) => {
  return (
    <section className="product-list">
      {productos.map((producto) => {
        return (
          <Form>
            <Row className="mb-3">
              <Col xs={7}>
                <Item key={producto.id} producto={producto} />
              </Col>
            </Row>
          </Form>
        );
      })}
    </section>
  );
};

export default ItemList;
