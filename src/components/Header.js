import NavBar from './Navbar';

function Header() {
    return (
        <header id="main-header">
            <h1>Erick's Commerce</h1>
            <NavBar/>            
        </header>
    )
}

export default Header