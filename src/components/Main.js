import React from "react";
import ItemDetailContainer from "./ItemDetailContainer";
import ItemListContainer from "./ItemListContainer";
import Carrito from "./Cart";
import { Route, Routes } from "react-router-dom";

const Main = (props) => {
  return (
    <main className="container">
      <Routes>
        <Route path="/" element={<ItemListContainer />} />
        <Route path="/categoria/:id" element={<ItemListContainer />} />
        <Route path="/carrito" element={<Carrito />} />
        <Route path="/articulo/:id" element={<ItemDetailContainer />} />
      </Routes>
    </main>
  );
};

export default Main;
