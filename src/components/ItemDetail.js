/* eslint-disable jsx-a11y/alt-text */
import { Form, Row, Col, Accordion } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import image from "../images/vino-detalle.png";
import ItemCount from "./ItemCount";
import { useContext, React, useState} from "react";
import { Link } from "react-router-dom"
import { context } from "./CartContext"

const ItemDetail = ({ producto }) => {
  const { id, title, price, stock } = producto;
  const [seleccionado, setSeleccionado] = useState(false);
  const {addItem} = useContext(context);

const onAdd = (unidadSeleccionada) => {
  console.log("On Add desde el ItemDetailContainer")
  if(unidadSeleccionada !== undefined){
    setSeleccionado(unidadSeleccionada)
  }
}
  return (
    <>
      <Form>
        <Row className="mb-3">
          <Col xs={7}>
            <img src={image} rounded="true" fluid="true"></img>
          </Col>
          <Col>
            <h1>{title}</h1>
            <h5>#{id}</h5>
            <h2>${price}</h2>
            <h5>Incluye envío</h5>
            <h5>
              <b>Características</b>
            </h5>
            <h6>Promoción no valida en el estado de México.</h6>
            Stock Disponible
            <br />
            <ItemCount initial={0} stock={stock} onAdd={onAdd} />
            {seleccionado ? <Link to="/carrito">Ir al carrito</Link> : <Link to="/">Seguir comprando</Link>}
            <br />
          </Col>
        </Row>
      </Form>
      <br />
      <Accordion defaultActiveKey="0" flush alwaysOpen>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <b>Descripción del producto</b>
          </Accordion.Header>
          <Accordion.Body>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>
            <b>Características</b>
          </Accordion.Header>
          <Accordion.Body>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>
            <b>Términos de entrega y devolución</b>
          </Accordion.Header>
          <Accordion.Body>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="3">
          <Accordion.Header>
            <b>Términos de entrega y devolución</b>
          </Accordion.Header>
          <Accordion.Body>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  );
};

export default ItemDetail;
